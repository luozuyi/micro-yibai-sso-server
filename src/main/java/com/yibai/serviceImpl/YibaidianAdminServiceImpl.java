package com.yibai.serviceImpl;

import com.yibai.entity.SysRes;
import com.yibai.entity.SysRole;
import com.yibai.entity.YibaidianAdmin;
import com.yibai.mapper.SysResMapper;
import com.yibai.mapper.SysRoleMapper;
import com.yibai.mapper.YibaidianAdminMapper;
import com.yibai.service.YibaidianAdminService;
import com.yibai.utils.Constants;
import com.yibai.utils.JwtToken;
import com.yibai.utils.Result;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Transactional
@Service
public class YibaidianAdminServiceImpl extends BaseServiceImpl<YibaidianAdmin, String> implements YibaidianAdminService {
    @Autowired
    private YibaidianAdminMapper yibaidianAdminMapper;
    @Resource
    private RedisTemplate<String, Object> redisTemplate;
    @Resource
    private HttpServletResponse response;
    @Autowired
    private SysRoleMapper sysRoleMapper;
    @Autowired
    private SysResMapper sysResMapper;
    @Override
    public Result adminLogin(String adminName, String password) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化值";
        try {
            if(StringUtils.isBlank(adminName)){
                code = "-2";
                msg = "登陆名不能为空";
            }else if(StringUtils.isBlank(password)){
                code = "-3";
                msg = "密码不能为空";
            }else{
                YibaidianAdmin admin = yibaidianAdminMapper.selectByAdminName(adminName);
                if(admin == null){
                    code = "-4";
                    msg = "用户名或密码不正确";
                }else if(!password.equals(admin.getPassword())){
                    code = "-4";
                    msg = "用户名或密码不正确";
                }else if("1".equals(admin.getDelFlag())){
                    code = "-5";
                    msg = "管理员已被禁用";
                }else{
                    String token = JwtToken.createAdminToken(admin);
                    token = token +"_"+ admin.getId()+":"+admin.getCoreAdminId();
                    redisTemplate.opsForValue().set(token, admin, 30, TimeUnit.DAYS);
                    code = "0";
                    msg = "成功";
                    Map<String,Object> map = new HashMap<String,Object>();
                    map.put("token",token);
                    map.put("admin", admin);
                    /**查询角色*/
                    String sysRoleId = admin.getSysRoleId();
                    SysRole sysRole = sysRoleMapper.selectByPrimaryKey(sysRoleId);
                    map.put("sysRole", sysRole);
                    /**查询权限列表*/
                    List<SysRes> sysResList = sysResMapper.selectByRoleId(sysRoleId);
                    map.put("sysResList", sysResList);
                    result.setData(map);
                    Cookie adminCookie=new Cookie("yibaiAdminToken",token);
                    adminCookie.setMaxAge(30*24*60*60);   //存活期为一个月 30*24*60*60
                    adminCookie.setPath("/");
                    response.addCookie(adminCookie);
                }
            }
        }catch (Exception e){
            code = "-5";
            msg = "系统异常";
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    @Override
    public Result cleanRedis(String token) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        if (StringUtils.isBlank(token)) {
            code = "-3";
            msg = "非法请求";
        } else {
            if(redisTemplate.hasKey(token)){
                redisTemplate.delete(token);
                code = Constants.SUCCESS;
                msg = "清除redis成功";
                Cookie adminCookie=new Cookie("yibaiAdminToken",token);
                adminCookie.setMaxAge(-1);   //将cookie时间清除
                adminCookie.setPath("/");
                response.addCookie(adminCookie);
            }else{
                code = "-4";
                msg = "已经退出";
            }
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result adminLoginCheck(String token, String method, String url) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        if (StringUtils.isBlank(token) || StringUtils.isBlank(method) || StringUtils.isBlank(url)) {
            code = "-3";
            msg = "非法请求";
        } else {
            Boolean flag = redisTemplate.hasKey(token);
            if (!flag) {
                code = "-4";
                msg = "已经过期";
            } else {
                String adminId = token.split("_")[1];
                YibaidianAdmin admin = yibaidianAdminMapper.selectByPrimaryKey(adminId);
                String roleId = admin.getSysRoleId();
                //查询当前用户的权限
                List<SysRes> sysResList = sysResMapper.selectByRoleId(roleId);
                //封装查询条件
                SysRes sysResParam = new SysRes();
                sysResParam.setMethod(method);
                sysResParam.setUrl(url);
                //当前请求res
                SysRes sysRes_db = sysResMapper.selectByUrlAndMethod(sysResParam);
                if(sysResList.contains(sysRes_db)){
                    code = Constants.SUCCESS;
                    msg = "成功";
                }else{
                    code = "-5";
                    msg = "无权限";
                }
            }
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result tokenCheck(String token) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        if (StringUtils.isBlank(token)) {
            code = "-3";
            msg = "非法请求";
        } else {
            Boolean flag = redisTemplate.hasKey(token);
            if (!flag) {
                code = "-4";
                msg = "已经过期";
            } else {
                code = Constants.SUCCESS;
                msg = "成功";
            }
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
