package com.yibai.serviceImpl;

import com.yibai.mapper.BaseMapper;
import com.yibai.service.BaseService;
import com.yibai.utils.Constants;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.io.Serializable;
@Transactional
@Service
public abstract class BaseServiceImpl<T,ID extends Serializable> implements BaseService<T, ID> {
    @Autowired
    private BaseMapper<T, ID> baseMapper;
    @Override
    public Result deleteByPrimaryKey(ID id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(id == null || id == ""){
                code = "-3";
                msg = "主键id不能为空";
            }else{
                T t = baseMapper.selectByPrimaryKey(id);
                if(t == null){
                    code = "-4";
                    msg = "删除对象不存在";
                }else{
                    baseMapper.deleteByPrimaryKey(id);
                    code = Constants.SUCCESS;
                    msg = "删除成功";
                }
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Override
    public Result selectByPrimaryKey(ID id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(id == null || id == ""){
                code = "-3";
                msg = "主键id不能为空";
            }else{
                T t = baseMapper.selectByPrimaryKey(id);
                result.setData(t);
                code = Constants.SUCCESS;
                msg = "成功";
            }
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Override
    public Result insert(T record) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            baseMapper.insert(record);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result insertSelective(T record) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            baseMapper.insertSelective(record);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result updateByPrimaryKeySelective(T record) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            baseMapper.updateByPrimaryKeySelective(record);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result updateByPrimaryKey(T record) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            baseMapper.updateByPrimaryKey(record);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
