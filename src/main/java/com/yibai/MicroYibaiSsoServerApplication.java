package com.yibai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroYibaiSsoServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroYibaiSsoServerApplication.class, args);
	}
}
