package com.yibai.service;

import com.yibai.utils.Result;

import java.io.Serializable;

public interface BaseService<T,ID extends Serializable> {

    Result deleteByPrimaryKey(ID id);

    Result insert(T record);

    Result insertSelective(T record);

    Result selectByPrimaryKey(ID id);

    Result updateByPrimaryKeySelective(T record);

    Result updateByPrimaryKey(T record);
}
