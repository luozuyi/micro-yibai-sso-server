package com.yibai.mapper;

import com.yibai.entity.SysRes;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface SysResMapper extends BaseMapper<SysRes,String>{
    List<SysRes> selectByRoleId(String roleId);

    SysRes selectByUrlAndMethod(SysRes record);
}