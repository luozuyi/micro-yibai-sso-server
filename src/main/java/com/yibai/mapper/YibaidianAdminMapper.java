package com.yibai.mapper;

import com.yibai.entity.YibaidianAdmin;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface YibaidianAdminMapper extends BaseMapper<YibaidianAdmin,String> {
    YibaidianAdmin selectByCoreAdminId(Long coreAdminId);

    YibaidianAdmin selectByAdminName(String adminName);
}